/*
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using UnityEngine;
using static UnityEditor.Searcher.SearcherWindow.Alignment;
using System.Numerics;
using UnityEngine.UIElements;
using Unity.VisualScripting;

public partial class PlayerMovementSystem : SystemBase
{
    protected override void OnCreate()
    {
        EntityQuery query = GetEntityQuery(typeof(PlayerTag));
    }

    protected override void OnStartRunning()
    {
        EntityQuery query = GetEntityQuery(typeof(PlayerTag));
        Entity player = query.GetSingletonEntity();
        PlayerManager.Instance.player = player;
        if (!HasComponent<VehicleRuntimeData>(player))
        {
            World.EntityManager.AddComponentData(player, new VehicleRuntimeData 
            { 
                currentFacingAngle = 0,
                forward = new float3(0, 0, 1)
            });
        }
    }

    protected override void OnUpdate()
    {
        // Assign values to local variables captured in your job here, so that it has
        // everything it needs to do its work when it runs later.
        // For example,
        //     float deltaTime = Time.DeltaTime;

        // This declares a new kind of job, which is a unit of work to do.
        // The job is declared as an Entities.ForEach with the target components as parameters,
        // meaning it will process all entities in the world that have both
        // Translation and Rotation components. Change it to process the component
        // types you want.
        float deltaTime = Time.DeltaTime;
        float horizontalAxisValue = Input.GetAxis("Horizontal");
        float verticalAxisValue = Input.GetAxis("Vertical");

        Entities
            .WithAll<PlayerTag>()
            .ForEach((ref Rotation rotation, ref PhysicsVelocity velocity, ref VehicleRuntimeData runtimeVehicle, in VehicleData vehicle) => 
            {
                horizontalAxisValue *= math.dot(runtimeVehicle.forward, velocity.Linear) < 0 ? -1f : 1f;

                float rotateValue = math.lengthsq(velocity.Linear) > 0 ? horizontalAxisValue : 0;
                float previousAngle = runtimeVehicle.currentFacingAngle;
                float resultAngle = runtimeVehicle.currentFacingAngle + rotateValue * deltaTime * vehicle.handling * math.length(velocity.Linear);
                resultAngle = resultAngle % 360f;
                runtimeVehicle.currentFacingAngle = resultAngle;

                // rotate forward vector to the angle e.g. rotate forward vector by 45 degree
                float3 forward = math.mul(quaternion.Euler(0, math.PI / 180 * runtimeVehicle.currentFacingAngle, 0), new float3(0,0,1));
                forward = math.normalize(forward);
                runtimeVehicle.forward = forward;

                float currentSpeedLength = math.length(velocity.Linear);
                float3 currentForwardSpeed = math.dot(velocity.Linear, runtimeVehicle.forward) < 0 ? currentSpeedLength * -forward : currentSpeedLength * forward;

                if (verticalAxisValue == 0)
                {
                    float3 resultSpeed = currentForwardSpeed - math.normalize(velocity.Linear) * (deltaTime * vehicle.deceleration);
                    velocity.Linear = math.lengthsq(velocity.Linear) > 1f ? resultSpeed : float3.zero;
                }
                else
                {
                    float3 acceleration = (forward * deltaTime * vehicle.acceleration);

                    if (verticalAxisValue < 0)
                    {
                        acceleration = -acceleration;
                    }
                    float3 newVelocity = acceleration + currentForwardSpeed;
                    float magnitude = math.length(newVelocity);
                    if (magnitude > vehicle.maxSpeed)
                    {
                        newVelocity = math.normalize(newVelocity) * vehicle.maxSpeed;
                    }
                    velocity.Linear = newVelocity;
                }

                if(horizontalAxisValue != 0)
                {
                    float3 lookAt = runtimeVehicle.forward;
                    rotation.Value = quaternion.LookRotationSafe(lookAt, new float3(0, 1, 0));
                }

            }).Schedule();
    }
}
*/
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Physics.Authoring;
using UnityEngine;

public struct PassengerPickUpData : IComponentData
{
    public Entity passenger;

    public int passengerCount;
}

public class PassengerPickUpAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    public PhysicsBodyAuthoring zone;
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new PassengerPickUpData
        {
            passengerCount = 0
        });
    }
}

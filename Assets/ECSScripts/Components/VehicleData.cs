using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct VehicleData : IComponentData
{
    public float acceleration;
    public float deceleration;
    public float brake;
    public float handbrake;
    public float handling;
    public float durability;
    public float gasolineConsumption;
    public float maxGasolineCapacity;
    public float currentGasoline;
    public float maxSpeed;
    public float maxTurnAngle;
}

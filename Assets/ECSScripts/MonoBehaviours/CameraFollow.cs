using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UIElements;
/*
public class CameraFollow : MonoBehaviour
{
    public static CameraFollow instance;

    float3 offset;
    float offsetMagnitude;
    quaternion rotationOffset;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        offset = new float3(0, transform.position.y, 0);
        offsetMagnitude = Vector3.Magnitude(new Vector3(transform.position.x, 0, transform.position.z));
        rotationOffset = transform.rotation;
    }

    private void LateUpdate()
    {
        Translation playerTranslation = PlayerManager.Instance.translation;
        VehicleRuntimeData playerVehicleRTD = PlayerManager.Instance.vehicleRTD;

        transform.position = playerTranslation.Value - (new float3(playerVehicleRTD.forward.x, 0, playerVehicleRTD.forward.z) * offsetMagnitude) + offset;
        quaternion targetRotation = Quaternion.LookRotation(playerVehicleRTD.forward, Vector3.up) * rotationOffset;
        transform.rotation = targetRotation;
    }
}
*/
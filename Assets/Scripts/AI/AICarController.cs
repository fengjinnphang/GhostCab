using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AICarController : Car
{
    public NavMeshAgent agent;

    public float minSpeed = 3.5f;
    public float maxSpeed = 10f;

    EnvironmentInfo currentDestination;

    void Update()
    {
        if(agent.remainingDistance < 1)
        {
            SetRandomDestination();
        }
    }

    public void SetRandomDestination()
    {
        if(MapManager.Instance == null)
        {
            return;
        }
        currentDestination = MapManager.Instance.GetRandomEnvironmentInfo(transform.position, 10);
        while(currentDestination == null)
        {
            currentDestination = MapManager.Instance.GetRandomEnvironmentInfo(transform.position, 10);
        }
        agent.SetDestination(currentDestination.GetPosition());
        agent.speed = Random.Range(minSpeed, maxSpeed);
    }
}

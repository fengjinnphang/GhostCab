using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Passenger : MonoBehaviour
{
    public enum PassengerTag
    {
        Human = 0,
        Ghost = 1
    }

    public PassengerTag myTag;
    public int minMoney = 1;
    public int maxMoney = 10;
    public int money {get; private set;}

    public EnvironmentInfo customDestination;
    public List<EnvironmentInfo> possibleDropOffPlaces;

    EnvironmentInfo destination;

    void FindDestination()
    {
        if(customDestination != null)
        {
            destination = customDestination;
            return;
        }

        EnvironmentInfo result = GetPossibleDropOffPoint(); 
        if(result == null)
        {
            result = MapManager.Instance.GetRandomEnvironmentInfo(transform.position, Random.Range(10, 100));
        }
        if(result != null)
        {
            destination = result;
            money = Random.Range(minMoney, maxMoney+1);
        }
    }
    
    EnvironmentInfo GetPossibleDropOffPoint()
    {
        if(possibleDropOffPlaces == null || possibleDropOffPlaces.Count <= 0)
        {
            return null;
        }
        int randomIndex = UnityEngine.Random.Range(0, possibleDropOffPlaces.Count);
        return possibleDropOffPlaces[randomIndex];
    }

    public void GetIntoCab()
    {
        gameObject.SetActive(false);
        FindDestination();
    }

    public EnvironmentInfo GetDestination()
    {
        return destination;
    }

    public void ReachDestination()
    {
        destination = null;
    }

    public bool IsAlive()
    {
        return gameObject.activeInHierarchy;
    }
}

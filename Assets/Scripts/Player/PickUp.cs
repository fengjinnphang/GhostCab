using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public LayerMask pickUpLayer;

    Coroutine pickUpRoutine;

    List<GameObject> currentOverlapPickUpObjs = new List<GameObject>();

    private void OnTriggerEnter(Collider other)
    {
        if(((1 << other.gameObject.layer) & pickUpLayer) > 0)
        {
            currentOverlapPickUpObjs.Add(other.gameObject);
            PickUpPassenger();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(((1 << other.gameObject.layer) & pickUpLayer) > 0)
        {
            currentOverlapPickUpObjs.Remove(other.gameObject);
        }
    }

    void PickUpPassenger()
    {
        if(PlayerManager.Instance.HasSlotForNewPassenger())
        {
            if(pickUpRoutine != null)
            {
                StopCoroutine(pickUpRoutine);
            }
            pickUpRoutine = StartCoroutine(PickUpUpdate());
        }
    }

    IEnumerator PickUpUpdate()
    {
        while(currentOverlapPickUpObjs.Count > 0 && PlayerManager.Instance.HasSlotForNewPassenger())
        {
            if(Input.GetKeyUp(KeyCode.E))
            {
                Passenger currentPassenger = currentOverlapPickUpObjs[0].GetComponent<Passenger>();
                currentPassenger.GetIntoCab();
                PlayerManager.Instance.AddPassenger(currentPassenger);
                currentOverlapPickUpObjs.RemoveAt(0);
            }
            yield return null;
        }

        currentOverlapPickUpObjs.Clear();
    }
}

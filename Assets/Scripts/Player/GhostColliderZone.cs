using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostColliderZone : MonoBehaviour
{
    public Collider ghostZoneCollider;
    public List<Collider> allCarColliders;

    public LayerMask passThroughLayer;

    List<Collider> ignoreColliders = new List<Collider>();
    List<Collider> currentCollidingColliders = new List<Collider>();

    void Awake()
    {
        PlayerManager.OnDeactivatedGhostMode += ExitGhostMode;
        PlayerManager.OnActivatedGhostMode += EnterGhostMode;
        ghostZoneCollider.enabled = false;
    }

    private void OnDestroy()
    {
        PlayerManager.OnDeactivatedGhostMode -= ExitGhostMode;
        PlayerManager.OnActivatedGhostMode -= EnterGhostMode;
    }

    void OnTriggerEnter(Collider other)
    {
        if(PlayerManager.Instance.inGhostMode && (passThroughLayer.value & (1 << other.gameObject.layer)) != 0)
        {
            foreach(Collider item in allCarColliders)
            {
                Physics.IgnoreCollision(item, other);
            }
            currentCollidingColliders.Add(other);
            ignoreColliders.Add(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        currentCollidingColliders.Remove(other);
    }

    void EnterGhostMode()
    {
        ghostZoneCollider.enabled = true;
    }

    void ExitGhostMode()
    {
        foreach(Collider item in currentCollidingColliders)
        {
            if(!item.gameObject.isStatic)
            {
                item.gameObject.SetActive(false);
            }
        }

        foreach(Collider item in ignoreColliders)
        {
            foreach(Collider carCollider in allCarColliders)
            {
                Physics.IgnoreCollision(carCollider, item, false);
            }
        }
        ignoreColliders.Clear();

        ghostZoneCollider.enabled = false;
    }
}

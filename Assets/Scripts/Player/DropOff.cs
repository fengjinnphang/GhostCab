using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropOff : MonoBehaviour
{
    public LayerMask dropOffTargetLayer;

    public DestinationIndicator destinationIndicator;

    Coroutine dropOffRoutine;

    private void OnTriggerEnter(Collider other)
    {
        if(((1 << other.gameObject.layer) & dropOffTargetLayer) > 0)
        {
            DropOffPassenger();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(((1 << other.gameObject.layer) & dropOffTargetLayer) > 0)
        {
            StopCoroutine(dropOffRoutine);
            PlayerManager.Instance.TurnOffPlayerCanvas();
        }
    }
    
    void DropOffPassenger()
    {
        if(destinationIndicator.HasPassengerReachedDestination())
        {
            if(dropOffRoutine != null)
            {
                StopCoroutine(dropOffRoutine);
            }
            dropOffRoutine = StartCoroutine(DropOffUpdate());
        }
    }

    IEnumerator DropOffUpdate()
    {
        PlayerManager player = PlayerManager.Instance;
        while(destinationIndicator.HasAssociatedPassenger())
        {
            player.ShowPlayerWithinPassengerDest();
            if(Input.GetKeyUp(KeyCode.E))
            {
                Passenger currentPassenger = destinationIndicator.GetAssociatedPassenger();
                destinationIndicator.Deactivate();
                player.DropOffPassenger(currentPassenger);
                player.TurnOffPlayerCanvas();
                break;
            }
            yield return null;
        }
    }
}

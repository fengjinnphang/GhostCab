using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationPointer : MonoBehaviour
{
    public float selfRotateSpeed = 100f;

    public Transform winZone;
    public bool isWinning {get; private set;}
    Transform destinationTransform;
    float timePassed;

    DestinationIndicator destinationIndicator;

    public void Initialize()
    {
        destinationIndicator = MapManager.Instance.destinationIndicator;
        destinationTransform = destinationIndicator.transform;
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void SetIsWinning(bool value)
    {
        if(gameObject.activeInHierarchy && isWinning && !value)
        {
            Deactivate();
        }
        isWinning = value;
        if(isWinning && !gameObject.activeInHierarchy)
        {
            Activate();
        }
    }

    void OnEnable()
    {
        timePassed = 0;
    }

    void Update()
    {
        Vector3 target = isWinning ? new Vector3(winZone.position.x, transform.position.y, winZone.position.z) : new Vector3(destinationTransform.position.x, transform.position.y, destinationTransform.position.z);
        transform.LookAt(target);
        transform.rotation = transform.rotation * Quaternion.AngleAxis(timePassed, Vector3.forward);
        timePassed += selfRotateSpeed * Time.deltaTime;
    }
}

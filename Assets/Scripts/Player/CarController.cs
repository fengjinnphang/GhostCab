using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AxleInfo {
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;
}
     
public class CarController : Car
{
    public Rigidbody rigidbody;
    public List<AxleInfo> axleInfos; 
    public float maxMotorTorque;
    public float maxSteeringAngle;
    public float maxBrakeTorque = 300f;

    void Update()
    {
/*
        if(Input.GetAxis("Vertical") != 0)
        {
            currentFuelAmount -= fuelConsumptionRatePerSecond * Time.deltaTime;
        }
*/
        if(Input.GetKeyDown(KeyCode.Q))
        {
            PlayerManager.Instance.ActivateGhostMode();
        }
        else if(Input.GetKeyUp(KeyCode.Q))
        {
            PlayerManager.Instance.DeactivateGhostMode();
        }
    }
    
    public void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        float brakeTorque = Input.GetKey(KeyCode.Space) ? maxBrakeTorque : 0;
     
        foreach (AxleInfo axleInfo in axleInfos) 
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (currentFuelAmount > 0 && axleInfo.motor) 
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
                axleInfo.leftWheel.brakeTorque = brakeTorque;
                axleInfo.rightWheel.brakeTorque = brakeTorque;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
    }

    public bool IsCarMoving()
    {
        return rigidbody.velocity.sqrMagnitude >= 1;
    }
}
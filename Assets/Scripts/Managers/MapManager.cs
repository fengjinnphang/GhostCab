using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public static MapManager Instance {get{return instance;}}
    static MapManager instance;

    static List<EnvironmentInfo> allEnvironmentInfos = new List<EnvironmentInfo>();

    public DestinationIndicator destinationIndicator;


    public float laneOffsetFromCenter = 0.65f;

    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }

        instance = this;
    }

    public void RegisterToAllEnvironmentInfos(EnvironmentInfo info)
    {
        allEnvironmentInfos.Add(info);
    }
    
    public EnvironmentInfo GetRandomEnvironmentInfo(Vector3 currentPosition, float minDistance, EnvironmentType specificType = EnvironmentType.All)
    {
        int possibleInfiniteLoopCount = 0;
        List<EnvironmentInfo> filteredList = allEnvironmentInfos.FindAll(x => specificType.HasFlag(x.environmentType));
        EnvironmentInfo result = filteredList[UnityEngine.Random.Range(0, filteredList.Count)];
        if(result == null)
        {
            return null;
        }
        while(Vector3.Distance(result.GetPosition(), currentPosition) < minDistance)
        {
            result = filteredList[UnityEngine.Random.Range(0, filteredList.Count)];
            if(possibleInfiniteLoopCount > 10)
            {
                Debug.LogError($"Stuck in GetRandomEnvironmentInfo Loop :: Find random environment with minDistance of {minDistance} at least 10 times! Returning null!");
                return null;
            }
        }
        return result;
    }

    public void ActivateDestinationIndicator(Vector3 position, Passenger passenger)
    {
        destinationIndicator.Activate(position, passenger);
    }
}

[Flags]
public enum EnvironmentType
{
    None = 0,
    Residency = 1,
    All = (None | Residency)
}

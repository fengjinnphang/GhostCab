using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance {get{return instance;}}
    static PlayerManager instance;

    public static Action<Passenger> OnPassengerReachedDestination;
    public static Action OnActivatedGhostMode;
    public static Action OnDeactivatedGhostMode;
    public static int maxPassengerCapacity {private set; get;}

    public Material mat;

    public DestinationPointer destinationPointer;
    public CarController car;
    public EnvironmentInfo currentEnvironment {get; private set;}

    [Tooltip("Define car center position on the lane. 0.5 meaning lane center +- 0.5f")]
    public float carHalfWidth = 0.6f;

    public float minGhostCurrencyFactor = 0.1f;
    public float maxGhostCurrencyFactor = 0.5f;

    public float followLaneRuleIncrement = 1f;
    public float followLaneRuleDecrement = -1f;

    public float maxHumanity = 100;
    public float ghostModeHumanityDecreaseRatePerSecond = 1f;

    public int enterGhostThreshold;
    public int escapeThreshold;

    public bool inGhostMode {private set; get;}

    public float humanityMeter {private set; get;}
    public float humanityPercentage => (humanityMeter / maxHumanity);
    
    public GameObject playerHeadCanvas;
    public Image canvasImage;
    public Sprite letPassengerOut;
    public Sprite headToExit;

    [SerializeField] List<Passenger> passengers = new List<Passenger>();

    bool isSpendingHumanity = false;
    bool isFollowingTrafficLightRule;

    Quaternion defaultQuaternion;

    static readonly int inGhostModeMatVariable = Shader.PropertyToID("_InGhostMode");

    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }

        instance = this;

        maxPassengerCapacity = 1;
        inGhostMode = false;

        defaultQuaternion = transform.rotation;
    }

    void Start()
    {
        destinationPointer.Initialize();
        humanityMeter = maxHumanity * 0.5f;

        playerHeadCanvas.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            transform.rotation = currentEnvironment.transform.rotation;
            transform.position = currentEnvironment.GetPosition();
        }
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            transform.rotation = defaultQuaternion;
        }

        if(humanityMeter <= 0)
        {
            MenuUI.Instance.GameOver();
        }

        if(!car.IsCarMoving())
        {
            return;
        }

        if(isSpendingHumanity && humanityMeter >= enterGhostThreshold)
        {
            AddToHumanity(-ghostModeHumanityDecreaseRatePerSecond * Time.deltaTime);
        }

        if(currentEnvironment != null && !currentEnvironment.isLawless)
        {
            if(currentEnvironment.isTrafficLight)
            {
                // Debug.Log(isFollowingTrafficLightRule + " :: following traffic light rule <----------------------");
                AddToHumanity(Time.deltaTime * (isFollowingTrafficLightRule ? followLaneRuleIncrement : followLaneRuleDecrement));
            }
            else
            {
                // Debug.Log(IsFollowingLaneRule() + " :: following lane rule <----------------------");
                AddToHumanity(Time.deltaTime * (IsFollowingLaneRule() ? followLaneRuleIncrement : followLaneRuleDecrement));
            }
        }
    }

    public void SetEnterEnvironment(EnvironmentInfo newEnvironmentInfo)
    {
        currentEnvironment = newEnvironmentInfo;
        if(currentEnvironment.isTrafficLight)
        {
            FollowTrafficLightRule();
            currentEnvironment.onTrafficLightChange += FollowTrafficLightRule;
        }
    }

    void FollowTrafficLightRule()
    {
        isFollowingTrafficLightRule = Vector3.Dot(currentEnvironment.trafficLightForward, transform.forward) < -0.1f;
    }

    bool IsFollowingLaneRule()
    {
        bool shouldGetRightLane = Vector3.Dot(transform.forward, currentEnvironment.GetForward()) >= 0;
        Vector3 correctLaneCenter = currentEnvironment.GetLaneCenterPosition(!shouldGetRightLane);
        // did not consider angle other than 0, 90, 180, 270
        float carMid;
        float laneMid;
        float dotResult = Vector3.Dot(currentEnvironment.GetForward(), Vector3.forward);
        if(dotResult > 0.2f || dotResult < -0.2f)
        {
            carMid = transform.position.x;
            laneMid = correctLaneCenter.x;
        }
        else
        {
            carMid = transform.position.z;
            laneMid = correctLaneCenter.z;
        }
        if(carMid > laneMid - carHalfWidth && carMid < laneMid + carHalfWidth)
        {
            return true;
        }

        return false;
    }

    public void ActivateGhostMode()
    {
        if(inGhostMode)
        {
            return;
        }
        EnterGhostMode();
        isSpendingHumanity = true;
    }

    public void DeactivateGhostMode()
    {
        ExitGhostMode();
        isSpendingHumanity = false;
    }

    public void AddToHumanity(float value)
    {
        humanityMeter += value;
        humanityMeter = Mathf.Clamp(humanityMeter, 0, maxHumanity);
        UpdateHumanGhostMeter();

        mat.SetFloat(inGhostModeMatVariable, inGhostMode ? 1 : 0);
        
        destinationPointer.SetIsWinning(AbleToWin());
        if(AbleToWin())
        {
            ShowPlayerMoveToWinZone();
        }
        else if(canvasImage.sprite == headToExit)
        {
            TurnOffPlayerCanvas();
        }
    }

    void EnterGhostMode()
    {
        inGhostMode = true;
        OnActivatedGhostMode?.Invoke();
    }

    void ExitGhostMode()
    {
        inGhostMode = false;
        OnDeactivatedGhostMode?.Invoke();
    }

    public bool HasSlotForNewPassenger()
    {
        return passengers.Count < maxPassengerCapacity;
    }

    public void AddPassenger(Passenger p)
    {
        passengers.Add(p);
        if(passengers.Count == 1)
        {
            MapManager.Instance.ActivateDestinationIndicator(p.GetDestination().GetPosition(), p);
            destinationPointer.Activate();
        }
    }

    public bool IsCurrentPassenger(Passenger target)
    {
        return passengers.Contains(target);
    }

    public void DropOffPassenger(Passenger target)
    {
        float value = target.myTag == Passenger.PassengerTag.Human ? target.money : target.money * UnityEngine.Random.Range(minGhostCurrencyFactor, maxGhostCurrencyFactor);
        AddToHumanity(value);

        OnPassengerReachedDestination?.Invoke(target);
        passengers.Remove(target);
        destinationPointer.Deactivate();

        if(passengers.Count > 0)
        {
            Passenger nextPassenger = passengers[0];
            MapManager.Instance.ActivateDestinationIndicator(nextPassenger.GetDestination().GetPosition(), nextPassenger);
            destinationPointer.Activate();
        }
    }

    void UpdateHumanGhostMeter()
    {
        if(Input.GetKey(KeyCode.Q) && humanityMeter >= enterGhostThreshold)
        {
            return;
        }

        if(humanityMeter >= enterGhostThreshold && inGhostMode)
        {
            ExitGhostMode();
            isSpendingHumanity = false;

        }
        else if(humanityMeter < enterGhostThreshold && !inGhostMode)
        {
            EnterGhostMode();
            isSpendingHumanity = false;
        }
    }

    public bool AbleToWin()
    {
        return humanityMeter >= escapeThreshold;
    }

    public void ShowPlayerWithinPassengerDest()
    {
        if(canvasImage.sprite == letPassengerOut)
        {
            return;
        }
        playerHeadCanvas.SetActive(true);
        canvasImage.sprite = letPassengerOut;
    }

    public void ShowPlayerMoveToWinZone()
    {
        playerHeadCanvas.SetActive(true);
        canvasImage.sprite = headToExit;
    }

    public void TurnOffPlayerCanvas()
    {
        playerHeadCanvas.SetActive(false);
        canvasImage.sprite = null;
    }
}

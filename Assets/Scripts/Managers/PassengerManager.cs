using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassengerManager : MonoBehaviour
{
    public static PassengerManager Instance {get{return instance;}}
    static PassengerManager instance;

    static List<Passenger> passengerPool = new List<Passenger>();

    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }

        instance = this;
    }

    public Passenger GetFromPool()
    {
        return passengerPool.Find(x => !x.IsAlive());
    }
}

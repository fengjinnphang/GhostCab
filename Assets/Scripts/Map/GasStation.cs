using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasStation : MonoBehaviour
{
    public LayerMask interactableLayers;

    List<CarInfo> interactingCars = new List<CarInfo>();

    Coroutine gasStationRoutine;

    class CarInfo
    {
        public Car car;
        public bool isDirty;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(((1 << other.gameObject.layer) & interactableLayers) > 0)
        {
            Car found = other.gameObject.GetComponent<Car>();
            if(found)
            {
                interactingCars.Add(new CarInfo{ car = found, isDirty = false});
            }

            if(gasStationRoutine == null)
            {
                gasStationRoutine = StartCoroutine(GasStationUpdate());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        interactingCars.RemoveAll(x => x.car.gameObject == other.gameObject);
    }

    IEnumerator GasStationUpdate()
    {
        while(interactingCars.Count > 0)
        {
            foreach(CarInfo item in interactingCars)
            {
                bool isTankFull = item.car.RefillFuelOverTime();
                if(isTankFull)
                {
                    item.isDirty = true;
                }
            }

            interactingCars.RemoveAll(x => x.isDirty);

            yield return null;
        }

        gasStationRoutine = null;
    }
}

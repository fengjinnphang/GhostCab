using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinZone : MonoBehaviour
{
    public GameObject door;
    public LayerMask winTargetLayer;
    void Update()
    {
        door.SetActive(!PlayerManager.Instance.AbleToWin());
    }

    void OnTriggerEnter(Collider other)
    {
        if(((1 << other.gameObject.layer) & winTargetLayer) > 0)
        {
            MenuUI.Instance.GameOver(true);
        }
    }
}

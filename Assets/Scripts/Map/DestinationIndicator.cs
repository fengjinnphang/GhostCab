using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationIndicator : MonoBehaviour
{
    public float moveSpeed = 1;
    public float minHeightFromGround = 1f;
    public float maxHeightFromGround = 3f;
    
    Vector3 lowestPosition, highestPosition;
    float timePassed;

    Passenger associatedPassenger;

    public void Activate(Vector3 targetPosition, Passenger passenger)
    {
        transform.position = targetPosition + new Vector3(0, minHeightFromGround, 0);
        lowestPosition = transform.position;
        highestPosition = targetPosition + new Vector3(0, maxHeightFromGround, 0);

        timePassed = 0;
        gameObject.SetActive(true);

        associatedPassenger = passenger;
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
        associatedPassenger = null;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(lowestPosition, highestPosition, Mathf.PingPong(timePassed, 1));
        timePassed += moveSpeed * Time.deltaTime;
    }

    public bool HasPassengerReachedDestination()
    {
        return PlayerManager.Instance.IsCurrentPassenger(associatedPassenger);
    }

    public bool HasAssociatedPassenger()
    {
        return associatedPassenger != null;
    }

    public Passenger GetAssociatedPassenger()
    {
        return associatedPassenger;
    }
}

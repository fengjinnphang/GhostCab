using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentInfo : MonoBehaviour
{
    public EnvironmentInfo positiveZ_Neighbour;
    public EnvironmentInfo negativeZ_Neighbour;
    public EnvironmentInfo positiveX_Neighbour;
    public EnvironmentInfo negativeX_Neighbour;
    
    public EnvironmentType environmentType;

    public bool isLawless = false;

    public Action onTrafficLightChange;

    [SerializeField] MeshFilter meshFilter;

    List<EnvironmentInfo> neighbours = new List<EnvironmentInfo>();

    Mesh mesh;

    float laneOffsetLength;

    bool isAlignedWithWorldForward;
    [HideInInspector] public bool isTrafficLight;
    [HideInInspector] public Vector3 trafficLightForward;

    PlayerManager player;

    void Awake()
    {
        // mesh = meshFilter.mesh;

        if(positiveZ_Neighbour != null)
        {
            neighbours.Add(positiveZ_Neighbour);
        }
        if(negativeZ_Neighbour != null)
        {
            neighbours.Add(negativeZ_Neighbour);
        }
        if(positiveX_Neighbour != null)
        {
            neighbours.Add(positiveX_Neighbour);
        }
        if(negativeX_Neighbour != null)
        {
            neighbours.Add(negativeX_Neighbour);
        }

        float angle = transform.rotation.eulerAngles.y;
        angle %= 360;
        angle = Mathf.Abs(angle);
        isAlignedWithWorldForward = angle == 0 || angle == 180 || angle == 360;
    }

    void Start()
    {
        MapManager map = MapManager.Instance;
        map.RegisterToAllEnvironmentInfos(this);
        laneOffsetLength = map.laneOffsetFromCenter;

        player = PlayerManager.Instance;
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject == player.gameObject)
        {
            player.SetEnterEnvironment(this);
        }
    }

    public Vector3 GetMeshBound()
    {
        return Vector3.zero;
       //  return mesh.bounds.size;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetForward()
    {
        return transform.forward;
    }

    public Vector3 GetLaneCenterPosition(bool isLeft)
    {
        int leftFactor = isLeft ? -1 : 1;
        Vector3 direction = (isAlignedWithWorldForward ? Vector3.right * -leftFactor : Vector3.forward * leftFactor);
        return transform.position + direction * laneOffsetLength + direction * 0.5f;
    }

    public EnvironmentInfo GetNextEnvironmentInfo(Vector3 forward)
    {
        if(neighbours.Count == 2)
        {
            foreach(EnvironmentInfo item in neighbours)
            {
                if(Vector3.Dot(item.GetPosition(), forward) > 0)
                {
                    return item;
                }
            }
        }
        return null;
    }
}

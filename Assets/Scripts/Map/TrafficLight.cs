using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : MonoBehaviour
{
    public enum PathType
    {
        Left = 0,
        Forward = 1,
        Right = 2,
        Backward = 3
    }

    public EnvironmentInfo environmentInfo;

    [Range(3, 4)]
    public int directionCount = 4;
    public float durationToSwitch = 4;
    public float lightIndicatorGap = 2f;
    public PathType notAPath;

    public Transform greenLightIndicator;
    public List<Transform> redLightIndicators;

    List<Vector3> forwardVectors = new List<Vector3>();

    int currentDirectionIndex;
    float elapsedTime;

    void Start()
    {
        currentDirectionIndex = Random.Range(0, directionCount);
        elapsedTime = 0;

        if(directionCount == 4)
        {
            forwardVectors.Add(Vector3.forward);
            forwardVectors.Add(Vector3.left);
            forwardVectors.Add(Vector3.right);
            forwardVectors.Add(Vector3.back);
        }
        else
        {
            switch(notAPath)
            {
                case PathType.Left:
                    forwardVectors.Add(Vector3.forward);
                    forwardVectors.Add(Vector3.right);
                    forwardVectors.Add(Vector3.back);
                    break;
                case PathType.Forward:
                    forwardVectors.Add(Vector3.left);
                    forwardVectors.Add(Vector3.right);
                    forwardVectors.Add(Vector3.back);
                    break;
                case PathType.Right:
                    forwardVectors.Add(Vector3.forward);
                    forwardVectors.Add(Vector3.left);
                    forwardVectors.Add(Vector3.back);
                    break;
                case PathType.Backward:
                    forwardVectors.Add(Vector3.forward);
                    forwardVectors.Add(Vector3.left);
                    forwardVectors.Add(Vector3.right);
                    break;
            }
        }
        
        LightIndicator();

        environmentInfo.isTrafficLight = true;
        environmentInfo.trafficLightForward = forwardVectors[currentDirectionIndex];
    }

    void Update()
    {
        if(elapsedTime >= durationToSwitch)
        {
            elapsedTime = 0;
            currentDirectionIndex += 1;
            currentDirectionIndex %= directionCount;
            LightIndicator();
            environmentInfo.trafficLightForward = forwardVectors[currentDirectionIndex];
            environmentInfo.onTrafficLightChange?.Invoke();
        }
        else
        {
            elapsedTime += Time.deltaTime;
        }
    }

    void LightIndicator()
    {
        Vector3 greenLightPos = transform.position + forwardVectors[currentDirectionIndex] * lightIndicatorGap;
        greenLightPos.y = greenLightIndicator.position.y;
        greenLightIndicator.position = greenLightPos;
        greenLightIndicator.LookAt(greenLightIndicator.position + forwardVectors[currentDirectionIndex]);

        int currentRedLightIndex = 0;

        for(int i=0; i<forwardVectors.Count; ++i)
        {
            if(i != currentDirectionIndex)
            {
                Vector3 pos = transform.position + forwardVectors[i] * lightIndicatorGap;
                pos.y = redLightIndicators[currentRedLightIndex].position.y;
                redLightIndicators[currentRedLightIndex].position = pos;
                redLightIndicators[currentRedLightIndex].LookAt(redLightIndicators[currentRedLightIndex].position + forwardVectors[i]);
                currentRedLightIndex += 1;
            }
        }
    }
    
}

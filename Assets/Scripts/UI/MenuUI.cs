using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
    public static MenuUI Instance {get{return instance;}}
    static MenuUI instance;

    public TMPro.TMP_Text title;
    public GameObject menuCanvas;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    public void GameOver(bool hasWon = false)
    {
        title.text = hasWon ? "You Have Became Human!" : "You Have Became Ghost!";
        menuCanvas.SetActive(true);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

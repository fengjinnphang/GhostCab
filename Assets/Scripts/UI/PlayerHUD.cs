using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
    public Image humanityBar;

    PlayerManager player;

    void Start()
    {
        player = PlayerManager.Instance;
    }

    void Update()
    {
        humanityBar.fillAmount = player.humanityPercentage;
        humanityBar.color = player.inGhostMode ? Color.blue : Color.green;
    }
}

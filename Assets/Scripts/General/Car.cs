using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public float fuelConsumptionRatePerSecond = 1f;
    public float fuelRefillRatePerSecond = 20f;

    public float currentFuelAmount = 100;
    public float maxFuelCapacity = 100;

    public float currentFuelPercentage => (currentFuelAmount / maxFuelCapacity);

    public bool RefillFuelOverTime()
    {
        currentFuelAmount += fuelRefillRatePerSecond * Time.deltaTime;
        currentFuelAmount = Mathf.Clamp(currentFuelAmount, 0, maxFuelCapacity);
        if(currentFuelAmount >= maxFuelCapacity)
        {
            return true;
        }

        return false;
    }
    
    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0) {
            return;
        }
     
        Transform visualWheel = collider.transform.GetChild(0);
     
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
     
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

}
